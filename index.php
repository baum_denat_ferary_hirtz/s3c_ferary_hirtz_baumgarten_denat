<?php
require 'vendor/autoload.php';
use \giftbox\controler\ControlerCatalogue;
use \giftbox\controler\ControlerMenu;
use \giftbox\controler\ControlerPanier;
use \giftbox\controler\ControlerCoffret;
use \giftbox\controler\ControlerAdmin;

session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/', function (){
	(new ControlerCatalogue())->accueil();
})->name('accueil');

$app->any('/pres/', function(){
    if(isset($_GET['tri'])){
        (new ControlerCatalogue())->prestations($_GET['tri']);
    }else{
        (new ControlerCatalogue())->prestations();
    }
})->name('prestations');
 
$app->get('/pres/:id/', function ($id){
    (new ControlerCatalogue())->prestation($id);
})->name('prestation');

$app->get('/cat/:cat_id/', function ($cat_id){
    if(isset($_GET['tri'])){
        (new ControlerCatalogue())->categorie($cat_id,$_GET['tri']);
    }else{
        (new ControlerCatalogue())->categorie($cat_id);
    }
})->name('categorie');

$app->get('/cat/', function(){
    (new ControlerCatalogue())->categories();
})->name('categories');

$app->get('/panier/', function(){
    (new ControlerPanier())->panier();
})->name('panier');

$app->get('/ajout/:id', function ($id){
    (new ControlerPanier())->ajoutPanier($id);
})->name('ajout');

$app->any('/validation/', function (){
    (new ControlerPanier())->validerPanier();
})->name('validation');

$app->get('/confirmation/', function (){
    (new ControlerPanier())->confirmerCommande();
})->name('confirmation');

$app->any('/paiement/', function (){
    (new ControlerPanier())->saisirPaiement();
})->name('paiement');

$app->get('/cadeau/:url/', function($id){
	(new ControlerCoffret())->ouvrirCoffret($id);
})->name('cadeau');

$app->any('/cagnotte/:url/', function($id){
	(new ControlerCoffret())->participerCagnotte($id);
})->name('cagnotte');

$app->any('/gestion/:url/', function($id){
	(new ControlerCoffret())->gererCoffret($id);
})->name('gestion');

$app->any('/connexion/:url/', function($id){
	(new ControlerCoffret())->verifierMdp($id);
})->name('connexion');

$app->get('/ajouterNote/:idPres/:note', function ($idPres,$note){
    (new ControlerCatalogue())->ajouterNote($idPres,$note);
})->name('ajoutNote');

$app->get('/modifQuantite/:id/:operation', function ($id,$operartion){
    (new ControlerPanier())->changerQuantite($id,$operartion);
})->name('modifQuantite');

$app->any('/admin/', function(){
    (new ControlerAdmin())->connexionAdmin();
})->name('admin');

$app->any('/deconnexionAdmin/', function(){
    (new ControlerAdmin())->deconnexion();
})->name('deconnexionAdmin');

$app->any('/admin/menu/', function(){
    (new ControlerAdmin())->menu();
})->name('adminMenu');

$app->any('/ajoutAdmin/', function(){
    (new ControlerAdmin())->ajoutPrestation();
})->name('ajoutAdmin');

$app->any('/admin/gestionDesactive/', function(){
    (new ControlerAdmin())->presDesactive();
})->name('gestionAdminDesactive');

$app->any('/admin/gestion/', function(){
    (new ControlerAdmin())->gestionAdmin();
})->name('gestionAdmin');

$app->any('/suppAdmin/:id', function($id){
    (new ControlerAdmin())->supprimer($id);
})->name('suppAdmin');

$app->any('/desactiveAdmin/:id', function($id){
    (new ControlerAdmin())->desactiver($id);
})->name('desactiveAdmin');

$app->any('/reactiveAdmin/:id', function($id){
    (new ControlerAdmin())->reactiver($id);
})->name('reactiveAdmin');

$app->run();
