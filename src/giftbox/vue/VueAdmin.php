<?php
/**
 * Created by PhpStorm.
 * User: thomasferary
 * Date: 17/01/2017
 * Time: 17:12
 */

namespace giftbox\vue;


class VueAdmin
{
    protected $array;
    protected $content;
    protected $name;

    public function __construct($table=NULL)
    {
        $this->array = $table;
    }

    private function connexion(){
        $this->name = "Connexion Administrateur";
        $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Connexion Administrateur</h3></div>";
        $res = $res."<form id=\"f1\" method=\"post\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f2\"> Mot de passe : </label>
					<input type=\"password\" id=\"f2\" name=\"mdp\" required>
				    </div>
				    <div class='col-md-12 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider\">
					</div>
					</form></div></div>";
        return $res;
    }

    private function menuAdmin(){
        $app=\Slim\Slim::getInstance();
        $this->name = "Menu Administrateur";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $route1 = $app->urlFor('gestionAdmin');
            $route2 = $app->urlFor('gestionAdminDesactive');
            $route3 = $app->urlFor('ajoutAdmin');
            $res = "<div class='container'><div class='row panier'>";
            $res = $res."<a href=".$route1."><div class='col-md-6 col-md-offset-3 col-xs-12 catPresentation'>Gestion des prestations</div></a>";
            $res = $res."<a href=".$route2."><div class='col-md-6 col-md-offset-3 col-xs-12 catPresentation'>Gestion des prestations désactivées</div></a>";
            $res = $res."<a href=".$route3."><div class='col-md-6 col-md-offset-3 col-xs-12 catPresentation'>Ajouter une prestation</div></a>";

        }
        else{
            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVad'><h3>Vous n'avez pas accès à cette page</h3></div></div></div>";
        }
        $res = $res."</div></div>";
        return $res;
    }

    private function gererPrestation(){
        $app=\Slim\Slim::getInstance();
        $this->name = "Gestion des prestations";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){


        $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Gestion des prestations</h3></div>";

        foreach ($this->array as $item){
            $route = $app->urlFor('suppAdmin',['id'=>$item->id]);
            $route2 = $app->urlFor('desactiveAdmin',['id'=>$item->id]);
            $cat = $item->categorie->nom;
            $res = $res."<div class='col-md-12 itemPanierVad'>
                         <b>$item->nom</b> - 
                         $cat - 
                         $item->prix €
                         </br><a href=\"$route\" class=\"supprimerPres colorRed\">Supprimer</a><a href=\"$route2\" class=\"supprimerPres colorYellow\">Desactiver</a>
                         </div>";
        }
        $res = $res."</div></div>";
        }
        else{
            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVad'><h3>Vous n'avez pas accès à cette page</h3></div></div></div>";
        }
        return $res;
    }

    private function presDesactive(){
        $app=\Slim\Slim::getInstance();
        $this->name = "Gestion des prestations";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){


            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Gestion des prestations désactivées</h3></div>";
            if(count($this->array) > 0) {
                foreach ($this->array as $item) {
                    $route = $app->urlFor('suppAdmin', ['id' => $item->id]);
                    $route2 = $app->urlFor('reactiveAdmin', ['id' => $item->id]);
                    $cat = $item->categorie->nom;
                    $res = $res . "<div class='col-md-12 itemPanierVad'>
                         <b>$item->nom</b> - 
                         $cat - 
                         $item->prix €
                         </br><a href=\"$route\" class=\"supprimerPres colorRed\">Supprimer</a><a href=\"$route2\" class=\"supprimerPres colorGreen\">Réactiver</a>
                         </div>";
                }
            }
            else{
                $res = $res . "<div class='col-md-12 itemPanierVad'><h3>Aucune prestation désactivée</h3></div>";
            }
            $res = $res."</div></div>";
        }
        else{
            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVad'><h3>Vous n'avez pas accès à cette page</h3></div></div></div>";
        }
        return $res;
    }

    private function ajoutPres(){
        $this->name = "Ajout de prestation";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)) {
            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Ajouter une prestation</h3></div>";

            $res = $res . "<form id=\"f1\" method=\"post\" enctype= \"multipart/form-data\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f1\">Nom de la prestation : </label>
					<input type=\"text\" id=\"name\" name=\"nom\" required>
					
					<label for=\"f1\">Catégorie de la prestation : </label>
					<input type=\"text\" id=\"cat_id\" name=\"cat_id\" required>
					
					<label for=\"f1\">Prix de la prestation : </label>
					<input type=\"text\" id=\"prix\" name=\"prix\" required>
					
					<label for=\"f1_msg\">Description de la prestation : </label>
					<textarea rows=2 cols=200 id=\"descr\" name=\"descr\" required></textarea><br>
				    
				    <label for=\"image\">Image : </label>
					<input type=\"file\" id=\"image\" name=\"image\" required>
					</div>
				    <div class='col-md-3 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider\">
					</div>
					</form></div></div>";
        }
        else{
            $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVad'><h3>Vous n'avez pas accès à cette page</h3></div></div></div>";
        }
        $res = $res."</div></div>";
        return $res;
    }

    public function render($num){
        switch ($num){
            case 1 :{
                $this->content = $this->connexion();
                break;
            }
            case 2 :{
                $this->content = $this->gererPrestation();
                break;
            }
            case 3 :{
                $this->content = $this->menuAdmin();
                break;
            }
            case 4 :{
                $this->content = $this->presDesactive();
                break;
            }
            case 5 :{
                $this->content = $this->ajoutPres();
                break;
            }
        }

        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('deconnexionAdmin');
        $routeBis = $app->urlFor('accueil');
        $routeMenu = $app->urlFor('adminMenu');
        $admin = $app->urlFor('admin');

        $urlCss = "http://".$_SERVER['SERVER_NAME']."/style.css";
        $urlBootstrap = "http://".$_SERVER['SERVER_NAME']."/bootstrap.min.css";
        $urlNav = "http://".$_SERVER['SERVER_NAME']."/nav.css";
        $urlScript = "http://".$_SERVER['SERVER_NAME']."/menu.js";
        $urlBack = "http://".$_SERVER['SERVER_NAME']."/back.jpg";
        $navModif = "";
        $menu = "";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $navModif = "<li class=\"anim\"><a href=\"$route\">Se deconnecter</a></li>";
            $menu = "<li class=\"anim\"><a href=\"$routeMenu\">Menu Administrateur</a></li>";
        }

        $html = <<<END

<!DOCTYPE html>
<html>

<head>
    <title>$this->name</title>
    <link rel="stylesheet" href="$urlBootstrap">
    <link rel="stylesheet" href="$urlCss">
    <link rel="stylesheet" href="$urlNav">
    <script type="text/javascript" src="$urlScript"></script>
    <meta name="viewport" content="width=375px, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> </head>
<body style="background-image: url($urlBack);">
<nav class="navigation" id="mobile">
    <div class="container">
        <ul>
            <ul>
            <li class="anim"> <a href="$routeBis">Giftbox</a></li>
            $menu
            $navModif
            <li><a onclick="responsive();" id="pointer"><span class="glyphicon glyphicon-circle-arrow-down" id="mobileLogo"></span></a></li>
        </ul>
        </ul>
    </div>
</nav>
    $this->content
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Site web réalisé par : <b>Mylene Hirtz</b> / <b>Thomas Ferary</b> / <b>Thomas Baumgarten</b> / <b>Martin Denat</b></br><a href="$admin" class='link'>Espace administrateur</a></p>
        <p class="text-muted"></p>
      </div>
    </footer>
</body>
</html>
END;
        echo $html;
    }
}