<?php

namespace giftbox\vue;
use \giftbox\model\Prestation;
use \giftbox\model\Coffret;
use \giftbox\model\Contient;
use \giftbox\model\Cagnotte;

class VueCoffret
{
    protected $coffret;
    protected $content;
    protected $name;

    public function __construct($c)
    {
        $this->coffret = $c;
    }
	
	function affichageCadeau(){
		$this->name = "Coffret cadeau";		
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Découvrez le coffret cadeau qui vous a été offert par ".$this->coffret->prenom." ".$this->coffret->nom." !</h3></div>";
		if($this->coffret->message != NULL){
			$res = $res."<div class='col-md-12 itemPanierVad'><h3>\"".$this->coffret->message."\"</h3></div>";
		}
		$res = $res."<div class='col-md-12 itemPanierVad'>Ce coffret contient : <ul>";
		$pres = Contient::where('idcoffret','=', $this->coffret->idcoffret)->get();
		foreach($pres as $p){
			$prestation = Prestation::where('id','=',$p->id)->get();
			foreach($prestation as $pr){
				$res = $res."<li>".$pr->nom." : ".$pr->descr."</li>";
			}
		}
		$this->coffret->etatcadeau = 1;
		$this->coffret->save();
		$res = $res."</ul></div></div></div>";
        return $res;
	}
	
	function affichageCagnotte(){
		$this->name = "Participation cagnotte";		
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Participez à la cagnotte du coffret cadeau créé par ".$this->coffret->prenom." ".$this->coffret->nom." !</h3></div>";
		$res = $res."<div class='col-md-12 itemPanierVad'><b>Ce coffret contient :</b> <ul>";
		$pres = Contient::where('idcoffret','=', $this->coffret->idcoffret)->get();
		$cagnotte = Cagnotte::where('urlcagnotte','=', $this->coffret->urlcagnotte)->first();
		foreach($pres as $p){
			$prestation = Prestation::where('id','=',$p->id)->get();
			foreach($prestation as $pr){
				$res = $res."<li><b>".$pr->nom."</b> : ".$pr->descr." <i>".$pr->prix."€</i></li>";
			}
		}               
		$res = $res."</ul></div><div class='col-md-12 itemPanierVad'><h3>Montant total : ".$this->coffret->prixtotal."€</h3> dont déjà <i>".$cagnotte->montant."€</i> récoltés</div>";
		
		if($cagnotte->cloture == 0){
			$res = $res."<form id=\"f1\" method=\"post\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f1\"> Montant de votre participation : </label>
					<input type=\"number\" id=\"f1\" name=\"participation\" required>
				    </div>
				    <div class='col-md-3 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider\">
					</div>
					</form>";
		}
		$res =$res."</div></div>";			
        return $res;
	}
	
	function verifierMdp(){
		$this->name = "Connexion espace de gestion du coffret";		
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Connectez-vous pour gérer votre coffret cadeau !</h3></div>";
		
		$res = $res."<form id=\"f1\" method=\"post\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f1\"> Mot de passe : </label>
					<input type=\"password\" id=\"f1\" name=\"mdp\" required>
				    </div>
				    <div class='col-md-3 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider\">
					</div>
					</form></div></div>";
        return $res;
	}
	
	function gererCoffret(){
		$this->name = "Gestion coffret";		
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Bienvenue ".$this->coffret->prenom." ".$this->coffret->nom." !</h3><br><h3>Vous pouvez ici gérer le coffret que vous avez créé</h3></div>";
		$res = $res."<div class='col-md-12 itemPanierVad'><b>Ce coffret contient : </b><ul>";
		$pres = Contient::where('idcoffret','=', $this->coffret->idcoffret)->get();
		foreach($pres as $p){
			$prestation = Prestation::where('id','=',$p->id)->get();
			foreach($prestation as $pr){
				$res = $res."<li><b>".$pr->nom."</b> : ".$pr->descr." <i>".$pr->prix."€</i></li>";
			}
		}
        if($this->coffret->modepaiement == 2){
            $cagnotte = Cagnotte::where('urlcagnotte','=', $this->coffret->urlcagnotte)->first();
            $dont = " dont déjà ".$cagnotte->montant."€ récoltés";
			$res = $res."</ul><h3>Montant total : ".$this->coffret->prixtotal."€</h3>$dont</div>";
        }
		else{
			$res = $res."</ul><h3>Montant total : ".$this->coffret->prixtotal."€</h3></div>";
		}
		if($this->coffret->etatpaiement == 1){
		switch($this->coffret->etatcadeau){
				case 0 :
				$res = $res."<div class='col-md-12 itemPanierVad'>Ce coffret n'a pas encore été ouvert</div>";
				break;
				case 1 :
				$res = $res."<div class='col-md-12 itemPanierVad'>Ce coffret a  été ouvert par le destinataire !</div>";
				break;
				case 2 :
				$res = $res."<div class='col-md-12 itemPanierVad'>Ce coffret a été seulement en partie ouvert</div>";
				break;
			}
		}
		if(($this->coffret->modepaiement == 2)&&($cagnotte->cloture == 0)){
			
			$res = $res."<form id=\"form\" method=\"post\" autocomplete=\"off\">
			        <div class='col-md-12 itemPanierVad'>
					<label for=\"montant\"> Montant de votre participation : </label>
					<input type=\"number\" id=\"montant\" name=\"montant\" required>
				    
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider\">
					</div>
					</form>";	
			if($cagnotte->montant >= $this->coffret->prixtotal){
				$res = $res."<form id=\"f2\" method=\"post\" >
				<div class='col-md-3 marginBottom bouttonPanier'>
				<input id='validationButton' type=\"submit\" name=\"cloturer\" value=\"Cloturer la cagnotte\">";
			}
			$res = $res."</div></form>";
		}		
		$res = $res."</p></form></div></div>";			
		
        return $res;
	}
	
	
	public function render($num, $panier = 0){
		 switch($num){
			case 1 :
				$this->content = $this->affichageCadeau();
				break;
			case 2 :
				$this->content = $this->affichageCagnotte();
				break;
			case 3 :
				$this->content = $this->verifierMdp();
				break;
			case 4 :
				$this->content = $this->gererCoffret();
				break;
		 }

        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('accueil');
        $routeDeco = $app->urlFor('deconnexionAdmin');
        $routeMenu = $app->urlFor('adminMenu');
        $admin = $app->urlFor('admin');

        $urlCss = "http://".$_SERVER['SERVER_NAME']."/style.css";
        $urlBootstrap = "http://".$_SERVER['SERVER_NAME']."/bootstrap.min.css";
        $urlNav = "http://".$_SERVER['SERVER_NAME']."/nav.css";
        $urlScript = "http://".$_SERVER['SERVER_NAME']."/menu.js";
        $urlBack = "http://".$_SERVER['SERVER_NAME']."/back.jpg";

        $navModif = "";
        $menu = "";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $navModif = "<li class=\"anim\"><a href=\"$routeDeco\">Se deconnecter</a></li>";
            $menu = "<li class=\"anim\"><a href=\"$routeMenu\">Menu Administrateur</a></li>";
        }

        $html = <<<END

        <!DOCTYPE html>
<html>
<head>
    <title>$this->name</title>
    <link rel="stylesheet" href="$urlBootstrap">
    <link rel="stylesheet" href="$urlCss">
    <link rel="stylesheet" href="$urlNav">
    <script type="text/javascript" src="$urlScript"></script>
    <meta name="viewport" content="width=375px, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> </head>
<body style="background-image: url($urlBack);">
<nav class="navigation" id="mobile">
    <div class="container">
        <ul>
            <li class="anim"> <a href="$route">Giftbox</a></li>
             $menu
            $navModif
            <li><a onclick="responsive();" id="pointer"><span class="glyphicon glyphicon-circle-arrow-down" id="mobileLogo"></span></a></li>
        </ul>
    </div>
</nav>
    
    $this->content
<footer class="footer">
      <div class="container">
        <p class="text-muted">Site web réalisé par : <b>Mylene Hirtz</b> / <b>Thomas Ferary</b> / <b>Thomas Baumgarten</b> / <b>Martin Denat</b></br><a href="$admin" class='link'>Espace administrateur</a></p>
        <p class="text-muted"></p>
      </div>
    </footer>
</body>
</html>
END;

echo $html;

    }
	
}