<?php

namespace giftbox\vue;
use \giftbox\model\Cagnotte;

class VuePanier
{
    protected $array;
    protected $content;
    protected $name;

    public function __construct($table,$q=NULL)
    {
        $this->array = $table;
        $this->quantite = $q;
    }

    private function affichagePanier(){
		$app=\Slim\Slim::getInstance();
        $all = $app->urlFor('prestations');
        $this->name = "Coffret";
        $res = "<div class='container panier'><div class='row'>";
		$total = 0;
		$cat = [];
		$nbpres = 0;
		$valider = $app->urlFor('validation');
        if(count($this->array) == 0){
            $res = $res . "<div class='col-md-12 itemPanierVide'>Votre panier est tristement <b>vide</b> ? <a href='$all' class='link'>Découvrez nos gammes de prestations</a></div>";
            $res = $res ."<h1 class='centrer triste'>:-(</h1>";
        }
        else{
            foreach ($this->array as $key => $value){
                $plus = $app->urlFor('modifQuantite',['id'=>$value->id,'operation'=>'plus']);
                $moins = $app->urlFor('modifQuantite',['id'=>$value->id,'operation'=>'moins']);
                $supp = $app->urlFor('modifQuantite',['id'=>$value->id,'operation'=>'supp']);
                $res = $res."<div class='col-md-12 itemPanier'><h3>".$value->nom."</h3>"."<h3>".$value->prix."€</h3>"."<p>Quantité : ".$this->quantite[$value->id]."</p><a href=\"$moins\" class=\"modifq\">-</a><a href=\"$plus\" class=\"modifq\">+</a><a href=\"$supp\" class=\"supprimerPanier\">Supprimer du panier</a>";
				$total = $total + ($value->prix * $this->quantite[$value->id]);
				array_push($cat, $value->cat_id);
				$nbpres = $nbpres + $this->quantite[$value->id];
				$res = $res."</div>";

            }
            $nbcat = count(array_count_values($cat));
            $res = $res."<div class='col-md-12 itemPanier'><h3>Total : ".$total."€ </h3>Au total : ".$nbpres." prestations de ".$nbcat." catégorie(s) (*)</div>";

            if($nbcat < 2) {
                $res = $res . "<div class='col-md-12 itemPanier'>(*) pour valider le coffret vous devez ajouter des prestations d'au moins deux catégories différentes</div>";
            }
            if($nbcat >= 2){
                $res = $res."<div class='col-md-3 itemValidation'><a href=\"$valider\" class=\"validation\">Valider le panier</a></div>";
            }
        }


		$res = $res."</div></div>";
        return $res;
    }

	private function validerPanier(){
		$app=\Slim\Slim::getInstance();
        $this->name = "Validation Coffret";
        $res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Récapitulatif de votre coffret</h3></div>";
		$total = 0;

        foreach ($this->array as $key => $value){
            $res = $res."<div class='col-md-12 itemPanier'>".$this->quantite[$value->id]." x  <b>".$value->nom."</b> - ".$value->prix."€ "."</div>";
			$total = $total + ($value->prix * $this->quantite[$value->id]);
        }
        		
		$res = $res."<div class='col-md-12 itemPanierVad'><h3>Total : ".$total."€ </h3>"."</div>";
		$res = $res."<div class='col-md-12 itemPanierVadHead'><h3>Validation de votre coffret</h3></div>";
		$res = $res."<form id=\"f1\" method=\"post\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f1_name\"> Nom : </label>
					<input type=\"text\" id=\"f1_name\" name=\"nom\" required>
					
					<label for=\"f1_firstname\"> Prénom : </label>
					<input type=\"text\" id=\"f1_firstname\" name=\"prenom\" required>
					
					<label for=\"f1_email\"> Adresse email : </label>
					<input type=\"email\" id=\"f1_email\" name=\"email\" required><br>
					
					
					<label for=\"f1_msg\"> Ajouter un message au coffret (optionnel) : </label>
					<textarea rows=2 cols=200 id=\"f1_msg\" name=\"message\" ></textarea><br>
					
					<label for=\"f1_mdp\"> Choisir un mot de passe pour la gestion de votre coffret : </label>
					<input type=\"password\" id=\"f1_mdp\" name=\"mdp\" required><br>
					</div>
					
					<div class='col-md-12 itemPanierVadHead'><h3>Mode de paiement :</h3></div>
                    <div class='col-md-12 itemPanierVad'>
					<input type=\"radio\" name=\"paiement\" value=\"1\" id=\"classique\" checked /><label class='block' for=\"classique\"> Paiement classique</label></br>
					<input type=\"radio\" name=\"paiement\" value=\"2\" id=\"cagnotte\" /><label class='block' for=\"cagnotte\"> Paiement par création d'une cagnotte</label>
				    </div>
					<input type=\"hidden\" name=\"total\" value=$total>
					<div class='col-md-3 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"valider\" value=\"Valider coffret\">
                    </div>
					</form>
					
					</div></div>";
					
		return $res;
	}
	
	function confirmerCommande(){
		$this->name = "Confirmation";
		$this->array['urlcadeau'] = filter_var($this->array['urlcadeau'], FILTER_SANITIZE_URL);
		$this->array['urlcagnotte'] = filter_var($this->array['urlcagnotte'], FILTER_SANITIZE_URL);
		$this->array['urlgestion'] = filter_var($this->array['urlgestion'], FILTER_SANITIZE_URL);
		$cagnotte = Cagnotte::where('urlcagnotte', '=', $this->array['urlcagnotte'])->first();
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Confirmation de votre achat</h3></div><div class='col-md-12 itemPanierVad'><h4>Votre achat a bien été effectué.</h4>";
				if(($this->array['urlcagnotte'] == NULL)||($cagnotte->cloture == 1)){
					$res = $res."L'url de votre coffret cadeau est : ".$this->array['urlcadeau']."<br>Vous pouvez dès à présent l'envoyer à son bénéficiaire !<br><br>";
				}
				else{
                    $url1 = $this->array['urlcagnotte'];
                    $res = $res."L'url de la cagnotte de participation à votre coffret est : <a href=\"$url1\" class='link'>".$this->array['urlcagnotte']."</a><br>";
				}
                $url2 = $this->array['urlgestion'];
				$res = $res."L'url de gestion de votre coffret est : <a href=\"$url2\" class='link'>".$this->array['urlgestion']."</a></div></div></div>";
		return $res;
	}
		
	function saisirPaiement(){
		$this->name = "Mode de paiement classique";
		$res = "<div class='container panier'><div class='row'><div class='col-md-12 itemPanierVadHead'><h3>Paiement du coffret</h3></div>";
		$res = $res."<div class='col-md-12 itemPanierVad'><h3>Total à payer : ".$this->array->prixtotal."€ </h3>"."</div>";
		$res = $res."<form id=\"f2\" method=\"post\" autocomplete=\"off\">
		            <div class='col-md-12 itemPanierVad'>
					<label for=\"f2_num\"> Numéro de carte bancaire : </label>
					<input type=\"number\" id=\"f2_num\" name=\"num\" required>
					
					<label for=\"f2_date\"> Date de validité : </label>
					<input type=\"date\" id=\"f2_date\" name=\"date\" required>
					
					<label for=\"f2_crypt\"> Cryptogramme : </label>
					<input type=\"number\" id=\"f2_crypt\" name=\"crypt\" required><br>
					</div>
					
					<div class='col-md-3 marginBottom bouttonPanier'>
					<input id='validationButton' type=\"submit\" name=\"validerpaiement\" value=\"Valider paiement\">				
				    </div>
					
					</form>
					
					</div></div>";
					
		return $res;
	}
	
     public function render($num, $panier = 0){
		 switch($num){
			 case 1 :
				$this->content = $this->affichagePanier();
				break;
			case 2 :
				$this->content = $this->validerPanier();
				break;
			case 3 :
				$this->content = $this->confirmerCommande();
				break;
			case 4 :
				$this->content = $this->saisirPaiement();
				break;
		 }

         $app=\Slim\Slim::getInstance();
         $route = $app->urlFor('accueil');
         $route1 = $app->urlFor('prestations');
         $route2 = $app->urlFor('categories');
         $route3 = $app->urlFor('panier');
         $routeDeco = $app->urlFor('deconnexionAdmin');
         $routeMenu = $app->urlFor('adminMenu');
         $admin = $app->urlFor('admin');

         $urlCss = "http://".$_SERVER['SERVER_NAME']."/style.css";
         $urlBootstrap = "http://".$_SERVER['SERVER_NAME']."/bootstrap.min.css";
         $urlNav = "http://".$_SERVER['SERVER_NAME']."/nav.css";
         $urlScript = "http://".$_SERVER['SERVER_NAME']."/menu.js";
         $urlBack = "http://".$_SERVER['SERVER_NAME']."/back.jpg";

         $navModif = "";
         $menu = "";
         if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
             $navModif = "<li class=\"anim\"><a href=\"$routeDeco\">Se deconnecter</a></li>";
             $menu = "<li class=\"anim\"><a href=\"$routeMenu\">Menu Administrateur</a></li>";
         }

        $html = <<<END

        <!DOCTYPE html>
<html>
<head>
    <title>$this->name</title>
    <link rel="stylesheet" href="$urlBootstrap">
    <link rel="stylesheet" href="$urlCss">
    <link rel="stylesheet" href="$urlNav">
    <script type="text/javascript" src="$urlScript"></script>
    <meta name="viewport" content="width=375px, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> </head>
<body style="background-image: url($urlBack);">
<nav class="navigation" id="mobile">
    <div class="container">
        <ul>
            <li class="anim"> <a href="$route">Giftbox</a></li>
            $menu
            $navModif
            <li class="anim"> <a href="$route2">Categories</a></li>
            <li class="anim"> <a href="$route1">Prestations</a></li>
            <li id="last" class="anim"> <a href="$route3">Coffret | $panier |</a></li>
            <li><a onclick="responsive();" id="pointer"><span class="glyphicon glyphicon-circle-arrow-down" id="mobileLogo"></span></a></li>
        </ul>
    </div>
</nav>
    
    $this->content
<footer class="footer">
      <div class="container">
        <p class="text-muted">Site web réalisé par : <b>Mylene Hirtz</b> / <b>Thomas Ferary</b> / <b>Thomas Baumgarten</b> / <b>Martin Denat</b></br><a href="$admin" class='link'>Espace administrateur</a></p>
        <p class="text-muted"></p>
      </div>
    </footer>
</body>
</html>
END;

echo $html;

    }
}