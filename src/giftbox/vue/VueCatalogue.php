<?php

namespace giftbox\vue;

use giftbox\model\Prestation;

class VueCatalogue
{
    protected $array;
    protected $content;
    protected $name;

    public function __construct($table)
    {
        $this->array = $table;
    }

    /**
     * @param string $tri
     * @return string
     */
    private function affichagePrestations($tri="aucun"){
        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('categories');
        $route2 = $app->urlFor('prestations');
        $attention = $route.'1/';
        $activite = $route.'2/';
        $restauration = $route.'3/';
        $hebergement = $route.'4/';
        $triCroissantPrix = $route2."?tri=triCroissantPrix";
        $triDecroissantPrix = $route2."?tri=triDecroissantPrix";
        $all = $app->urlFor('prestations');

        $this->name = "Prestations";

        $res = "<div class='container'><div class='filter col-md-12'>

<h5>Prestations</h5>
<select class =\"mySelect\"onChange=\"window.location.href=this.value\">
    <option value=\"\">Choisir catégorie</option>
    <option value=\"$all\">Aucune</option>
    <option value=\"$attention\">Attention</option>
    <option value=\"$activite\">Activité</option>
    <option value=\"$restauration\">Restauration</option>
    <option value=\"$hebergement\">Hebergement</option>
	</select>";

        $res = $res."<select class =\"mySelect\"onChange=\"window.location.href=this.value\">
    <option>Filtre</option>
    <option value=\"$route2\">Aucun</option>
    <option value=\"$triCroissantPrix\">Prix croissant</option>
    <option value=\"$triDecroissantPrix\">Prix decroissant</option>
	</select></div></div></div>";

        $res = $res."<div class='container'><div class='row'>";


        foreach ($this->array as $item){
            $note = $item->note;
            $notation = "<div>";
            if(count($note) > 0){
                $ens_note=0;
                foreach($note as $cle=>$value)
                {
                    $ens_note+=$value['note'];
                }
                $note_moy=$ens_note/count($note);
                $note_moy = intval($note_moy);

                for ($i = 1; $i <= $note_moy; $i++) {
                    $notation = $notation."<span class='circle-active'></span>";
                }
                for ($i = 1; $i <= 5 - $note_moy; $i++) {
                    $notation = $notation."<span class='circle-inactive'></span>";
                }
            }
            else{
                $notation = $notation."<span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span>";
            }
            $notation = $notation."</div>";


            $cat = $item->categorie->nom;
			$routepres = $app->urlFor('prestations');
			$ajout = $app->urlFor('ajout',['id'=>$item->id]);
			$urlpresid = $routepres.$item->id;
            $url = "http://".$_SERVER['SERVER_NAME']."/img/".$item->img;
            $res = $res."<div class=\"col-md-3\">
              
                    <div class=\"pres\"><a class=\"selection\" href=\"$urlpresid\"><img class=\"img\" src=\"$url\"></a>
                        <h2 class=\"img-text\">$item->prix €</h2>
                        <div class=\"pres-body\">
                            <h3 class=\"pres-title\">$item->nom</h3>
                            $notation
                            <h5 class=\"pres-title\">$cat</h5>
                            <a href=\"$ajout\" class=\"ajout\">Ajouter au panier</a>
                            </div>
                    </div>
                
            </div>";
        }
        $res = $res."</div></div>";
        return $res;
    }

    private function removeVarUrl($url, $varname) {
        list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
        parse_str($qspart, $qsvars);
        unset($qsvars[$varname]);
    return $urlpart;
}

    private function affichageCategorie($tri="aucun"){
        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('categories');
        $route1 = $route.'1/';
        $route2 = $route.'2/';
        $route3 = $route.'3/';
        $route4 = $route.'4/';
        $all = $app->urlFor('prestations');

        $this->name = "Catégorie";

        $monUrl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $monUrl = $this->removeVarUrl($monUrl,"tri");

        $triCroissantPrix = $monUrl."?tri=triCroissantPrix";
        $triDecroissantPrix = $monUrl."?tri=triDecroissantPrix";


        $res = "<div class='container'><div class='filter col-md-12'>
<h5>Prestations</h5>
<select class =\"mySelect\"onChange=\"window.location.href=this.value\">
    <option value=\"\">Choisir une catégorie</option>
    <option value=\"$all\">Aucune</option>
    <option value=\"$route1\">Attention</option>
    <option value=\"$route2\">Activité</option>
    <option value=\"$route3\">Restauration</option>
    <option value=\"$route4\">Hebergement</option>
	</select>";

        $res = $res."<select class =\"mySelect\"onChange=\"window.location.href=this.value\">
    <option>Choisir un filtre</option>
    <option value=\"$route2\">Aucun</option>
    <option value=\"$triCroissantPrix\">Prix croissant</option>
    <option value=\"$triDecroissantPrix\">Prix decroissant</option>
	</select></div></div></div>";

        $res = $res."<div class='container'><div class='row'>";


        foreach ($this->array as $item){
            $note = $item->note;
            $notation = "<div>";
            if(count($note) > 0){
                $ens_note=0;
                foreach($note as $cle=>$value)
                {
                    $ens_note+=$value['note'];
                }
                $note_moy=$ens_note/count($note);
                $note_moy = intval($note_moy);

                for ($i = 1; $i <= $note_moy; $i++) {
                    $notation = $notation."<span class='circle-active'></span>";
                }
                for ($i = 1; $i <= 5 - $note_moy; $i++) {
                    $notation = $notation."<span class='circle-inactive'></span>";
                }
            }
            else{
                $notation = $notation."<span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span>";
            }
            $notation = $notation."</div>";
            $cat = $item->categorie->nom;
            $routepres = $app->urlFor('prestations');
            $ajout = $app->urlFor('ajout',['id'=>$item->id]);
            $urlpresid = $routepres.$item->id;
            $url = "http://".$_SERVER['SERVER_NAME']."/img/".$item->img;
            $res = $res."<div class=\"col-md-3\">
              
                    <div class=\"pres\"><a class=\"selection\" href=\"$urlpresid\"><img class=\"img\" src=\"$url\"></a>
                        <h2 class=\"img-text\">$item->prix €</h2>
                  
                        <div class=\"pres-body\">
                            <h3 class=\"pres-title\">$item->nom</h3>
                            $notation
                            <h5 class=\"pres-title\">$cat</h5>
                            <a href=\"$ajout\" class=\"ajout\">Ajouter au panier</a>
                            </div>
                    </div>
                
            </div>";
        }
        $res = $res."</div></div>";
        return $res;
    }

    private function affichageCategories(){
		$app=\Slim\Slim::getInstance();
        $this->name = "Categories";
        $res = "<div class='container'>
        <div class='row cat'><div class='col-md-12 titre'><h3>Une envie particulière ? Parcourez nos gammes de prestations</h3></div>";
        foreach ($this->array as $item){
			$routecat = $app->urlFor('categories');
			$urlcatid = $routecat.$item->id;
            $res = $res."<a href=".$urlcatid."><div class='col-md-6 col-md-offset-3 col-xs-12 catPresentation'>".$item->nom."</div></a>";
        }
        $res = $res."</div></div>";
        return $res;
    }

   private function affichagePrestation(){
       $app=\Slim\Slim::getInstance();

       $note1 = $app->urlFor('ajoutNote',['idPres'=>$this->array->id,'note'=>1]);
       $note2 = $app->urlFor('ajoutNote',['idPres'=>$this->array->id,'note'=>2]);
       $note3 = $app->urlFor('ajoutNote',['idPres'=>$this->array->id,'note'=>3]);
       $note4 = $app->urlFor('ajoutNote',['idPres'=>$this->array->id,'note'=>4]);
       $note5 = $app->urlFor('ajoutNote',['idPres'=>$this->array->id,'note'=>5]);

       $lienNote[] = $note1;
       $lienNote[] = $note2;
       $lienNote[] = $note3;
       $lienNote[] = $note4;
       $lienNote[] = $note5;

       $note = $this->array->note;
        $notation = "";
       if(count($note) > 0){
           $notation = "<div class=\"rating\">";
           $ens_note=0;
           foreach($note as $cle=>$value)
           {
               $ens_note+=$value['note'];
           }
           $note_moy=$ens_note/count($note);
           $note_moy = intval($note_moy);


           for ($i = 0; $i < 5 - $note_moy; $i++) {
               $notation = $notation."<a class='inactive' href=\"$lienNote[$i]\">●</a>";
           }
           for ($i = 0; $i < $note_moy; $i++) {
               $it = 4 - $i;
               $notation = $notation."<a class='active' href=\"$lienNote[$it]\">●</a>";
           }
           $notation = $notation."</div>";
       }
       else{
           $notation = $notation = "<div class=\"rating\">
<a class='inactive' href=\"$note5\">●</a><a class='inactive' href=\"$note4\">●</a><a class='inactive' href=\"$note3\">●</a><a class='inactive' href=\"$note2\">●</a><a class='inactive' href=\"$note1\">●</a>
</div>";
       }


		$this->name = "Prestation";
		$url = "http://".$_SERVER['SERVER_NAME']."/img/".$this->array->img;
		$ajout = $app->urlFor('ajout',['id'=>$this->array->id]);


        $res = "<div class='container'><div class='row'><div class='col-md-12 presAllInfo'><div class='row'>";
        $res = $res."<div class='col-md-3 col-xs-12'><img class=\"imgPres\" src=\"$url\"></div>";
        $res = $res."<div class='col-md-9 col-xs-12 presAll'><h3>".$this->array->nom."</h3>$notation<p>".$this->array->descr."</p><h2>".$this->array->prix."€</h2></div></div></div><div class='col-md-4 col-xs-6 presAjoutInfo centrer'><a href=\"$ajout\" class=\"ajout\">Ajouter au panier</a></div></div></div>";
        return $res;
    }
	
	private function affichageAccueil(){
        $app=\Slim\Slim::getInstance();
        $this->name = "Accueil";
        $res = "<div class='container'>
        <div class='row cat'><div class='col-md-12 titre'><h3>Bienvenue sur Giftbox</h3><h4>Faites plaisir à vos proches !</h4></div>
        </div></div>";
        foreach ($this->array as $key => $item){

            $res = $res . "<div class='container'><div class='row'>";
            if($key == 0){
                $res = $res."<div class='col-md-12 subDiv'><h3 class='subTitre'>Découvrez nos meilleures prestations !</h3></div>";
            }
            if($key == 1){
                $res = $res."<div class='col-md-12 subDiv'><h3 class='subTitre'>En manque d'idées ?</h3></div>";
            }
            foreach ($this->array[$key] as $item) {
                $note = $item->note;
                $notation = "<div>";
                if (count($note) > 0) {
                    $ens_note = 0;
                    foreach ($note as $cle => $value) {
                        $ens_note += $value['note'];
                    }
                    $note_moy = $ens_note / count($note);
                    $note_moy = intval($note_moy);

                    for ($i = 1; $i <= $note_moy; $i++) {
                        $notation = $notation . "<span class='circle-active'></span>";
                    }
                    for ($i = 1; $i <= 5 - $note_moy; $i++) {
                        $notation = $notation . "<span class='circle-inactive'></span>";
                    }
                } else {
                    $notation = $notation . "<span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span><span class='circle-inactive'></span>";
                }
                $notation = $notation . "</div>";
                $cat = $item->categorie->nom;
                $routepres = $app->urlFor('prestations');
                $ajout = $app->urlFor('ajout', ['id' => $item->id]);
                $urlpresid = $routepres . $item->id;
                $url = "http://" . $_SERVER['SERVER_NAME'] . "/img/" . $item->img;

                $res = $res . "<div class=\"col-md-3\">
              
                    <div class=\"pres\"><a class=\"selection\" href=\"$urlpresid\"><img class=\"img\" src=\"$url\"></a>
                     
                        <div class=\"pres-body\">
                            <h3 class=\"pres-title\">$item->nom</h3>
                            $notation
                            <h5 class=\"pres-title\">$cat</h5>
                          
                            </div>
                    </div>
                
            </div>";
            }
            $res = $res . "</div></div>";

        }

        return $res;
    }

    public function render($num,$panier=0,$tri=NULL){
        switch ($num){
            case 1 :{
                $this->content = $this->affichagePrestations($tri);
                break;
            }
            case 2 :{
                $this->content = $this->affichagePrestation();
                break;
            }
            case 3 :{
                $this->content = $this->affichageCategorie($tri);
                break;
            }
            case 4 :{
                $this->content = $this->affichageCategories();
                break;
            }
			case 5 :{
                $this->content = $this->affichageAccueil();
                break;
            }
        }

        $app=\Slim\Slim::getInstance();
		$route = $app->urlFor('accueil');
        $route1 = $app->urlFor('prestations');
        $route2 = $app->urlFor('categories');
		$route3 = $app->urlFor('panier');
        $routeDeco = $app->urlFor('deconnexionAdmin');
        $routeMenu = $app->urlFor('adminMenu');
        $admin = $app->urlFor('admin');

        $urlCss = "http://".$_SERVER['SERVER_NAME']."/style.css";
        $urlBootstrap = "http://".$_SERVER['SERVER_NAME']."/bootstrap.min.css";
        $urlNav = "http://".$_SERVER['SERVER_NAME']."/nav.css";
        $urlScript = "http://".$_SERVER['SERVER_NAME']."/menu.js";
        $urlBack = "http://".$_SERVER['SERVER_NAME']."/back.jpg";

        $navModif = "";
        $menu = "";
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $navModif = "<li class=\"anim\"><a href=\"$routeDeco\">Se deconnecter</a></li>";
            $menu = "<li class=\"anim\"><a href=\"$routeMenu\">Menu Administrateur</a></li>";
        }


        $html = <<<END

<!DOCTYPE html>
<html>

<head>
    <title>$this->name</title>
    <link rel="stylesheet" href="$urlBootstrap">
    <link rel="stylesheet" href="$urlCss">
    <link rel="stylesheet" href="$urlNav">
    <script type="text/javascript" src="$urlScript"></script>
    <meta name="viewport" content="width=375px, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> </head>
<body style="background-image: url($urlBack);">
<nav class="navigation" id="mobile">
    <div class="container">
        <ul>
            <li class="anim"> <a href="$route">Giftbox</a></li>
            $menu
            $navModif
            <li class="anim"> <a href="$route2">Categories</a></li>
            <li class="anim"> <a href="$route1">Prestations</a></li>
            <li id="last" class="anim"> <a href="$route3">Coffret | $panier |</a></li>
            <li><a onclick="responsive();" id="pointer"><span class="glyphicon glyphicon-circle-arrow-down" id="mobileLogo"></span></a></li>
        </ul>
    </div>
</nav>
    $this->content
    
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Site web réalisé par : <b>Mylene Hirtz</b> / <b>Thomas Ferary</b> / <b>Thomas Baumgarten</b> / <b>Martin Denat</b></br><a href="$admin" class='link'>Espace administrateur</a></p>
        <p class="text-muted"></p>
      </div>
    </footer>
</body>
</html>
END;

echo $html;
    }
}