<?php

namespace giftbox\controler;
use \giftbox\model\Categorie;
use \giftbox\model\Prestation;
use \giftbox\model\Coffret;
use \giftbox\model\Contient;
use \giftbox\model\Cagnotte;
use \giftbox\vue\VueCoffret;


class ControlerCoffret{
	
	function ouvrirCoffret($id){
		$id = filter_var($id, FILTER_SANITIZE_STRING);
		$url = "http://".$_SERVER['SERVER_NAME']."/cadeau/".$id;
		$c = Coffret::where('urlcadeau', '=', $url)->first();
		$vue = new VueCoffret($c);
		$vue->render(1);
	}
	
	function participerCagnotte($id){
		$app=\Slim\Slim::getInstance();
		$id = filter_var($id, FILTER_SANITIZE_STRING);
		$url = "http://".$_SERVER['SERVER_NAME']."/cagnotte/".$id;
		$c = Coffret::where('urlcagnotte', '=', $url)->first();
		$vue = new VueCoffret($c);
		$vue->render(2);
		if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
			$participation = filter_var($app->request->post('participation'), FILTER_SANITIZE_NUMBER_FLOAT);
			if($participation > 0){
				$cagnotte = Cagnotte::where('urlcagnotte','=', $url)->first();
				$cagnotte->montant = $cagnotte->montant+$participation;
				$cagnotte->save();
			}
			$app->redirect($url);
		}
	}
	
	function verifierMdp($id){
		$app=\Slim\Slim::getInstance();
		$c = NULL;
		$vue = new VueCoffret($c);		
		if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
			$mdp = filter_var($app->request->post('mdp'), FILTER_SANITIZE_STRING);
			$id = filter_var($id, FILTER_SANITIZE_STRING);
			$url = "http://".$_SERVER['SERVER_NAME']."/connexion/".$id;
			$c = Coffret::where('urlgestion', '=', $url)->first();
			$idc = $c->idcoffret;
			$urlgestion = "http://".$_SERVER['SERVER_NAME']."/gestion/".$idc;
			if(password_verify($mdp,$c->mdp)){
				$app->redirect($urlgestion);
			}
			else{
				$vue->render(3);
			}
		}
		else{
			$vue->render(3);
		}
	}
	function gererCoffret($id){
		$app=\Slim\Slim::getInstance();	
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
		$c = Coffret::where('idcoffret', '=', $id)->first();
		$urlg = "http://".$_SERVER['SERVER_NAME']."/gestion/".$id;
		$url = $c->urlcagnotte;
		$vue = new VueCoffret($c);
		$vue->render(4);
		$cagnotte = Cagnotte::where('urlcagnotte', '=', $c->urlcagnotte)->first();
		if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
			if($app->request->post('cloturer') == 'Cloturer la cagnotte'){
				$c->etatpaiement = 1;
				$c->save();
				$cagnotte->cloture = 1;
				$cagnotte->save();
				$confirmation = $app->urlFor('confirmation');
				$app->redirect($confirmation);
			}
			
			else if($app->request->post('valider') == 'Valider'){					
					$participation = filter_var($app->request->post('montant'), FILTER_SANITIZE_NUMBER_FLOAT);
					if($participation > 0){
						$cagnotte->montant = $cagnotte->montant+$participation;
						$cagnotte->save();
					}
				$app->redirect($urlg);
			}
		}
	}	
	
}