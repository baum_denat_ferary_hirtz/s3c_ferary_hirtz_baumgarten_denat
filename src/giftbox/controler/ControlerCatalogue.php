<?php
 namespace giftbox\controler;
 use \giftbox\model\Categorie;
 use giftbox\model\Notation;
 use \giftbox\model\Prestation;
 use giftbox\model\PrestationDesactive;
 use \giftbox\vue\VueCatalogue;

 class ControlerCatalogue {

     public function prestations($tri=NULL){
		$app=\Slim\Slim::getInstance();
		$tri = filter_var($tri, FILTER_SANITIZE_STRING);
        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
             $id = $app->request->post('idpres');
             $this->ajout($id);
        }
        if($tri == NULL){
             $l = Prestation::all();
        }
        else if($tri == 'triCroissantPrix'){
             $l = Prestation::orderBy('prix','ASC')->get();
        }
        else if($tri == 'triDecroissantPrix'){
             $l = Prestation::orderBy('prix','DESC')->get();
        }
        $vue = new VueCatalogue($l);
        if(isset($_SESSION['panier'])){
             $vue->render(1,count($_SESSION['panier']),$tri);
        }
        else{
             $vue->render(1,0,$tri);
        }
     }

       public function prestation($id){
		 $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
         $l = Prestation::where('id','=',$id)->first();
         $vue = new VueCatalogue($l);
         if(isset($_SESSION['panier'])){
             $vue->render(2,count($_SESSION['panier']));
         }
         else{
             $vue->render(2);
		 }
     }

     public function categorie($cat_id,$tri=NULL){
		$cat_id = filter_var($cat_id, FILTER_SANITIZE_NUMBER_INT);
		$tri = filter_var($tri, FILTER_SANITIZE_STRING);
         if($tri == NULL){
             $l = Prestation::select()->where('cat_id','=',$cat_id)->get();
         }
         else if($tri == 'triCroissantPrix'){
             $l = Prestation::select()->where('cat_id','=',$cat_id)->orderBy('prix','ASC')->get();
         }
         else if($tri == 'triDecroissantPrix'){
             $l = Prestation::select()->where('cat_id','=',$cat_id)->orderBy('prix','DESC')->get();
         }
         $vue = new VueCatalogue($l);
         if(isset($_SESSION['panier'])){
             $vue->render(3,count($_SESSION['panier']));
         }else{
             $vue->render(3);
         }
     }
		 
	  public function categories(){
         $l = Categorie::all() ;
          $vue = new VueCatalogue($l);
		 if(isset($_SESSION['panier'])){
             $vue->render(4,count($_SESSION['panier']));
         }
         else{
             $vue->render(4);
         }
     }
	 
	 public function ajout($id){
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
		$l = Prestation::where('id','=',$id)->first();
		
		if(!isset($_SESSION)){
			session_start();
			$_SESSION[ 'panier' ] = [$l];
			echo 'creation session';
		}
		else{
			$_SESSION[ 'panier' ] = [$l];
			echo 'ajout panier';
		}
	}
		 
	 public function accueil(){
	     $table = array();
         $nb_a_tirer = 4;
         $val_min = 1;
         $nb = count(Prestation::all()) + count(PrestationDesactive::all());
         $val_max = $nb;
         $tab_result = array();
         while($nb_a_tirer != 0 )
         {
             $nombre = mt_rand($val_min, $val_max);
             if( !in_array($nombre, $tab_result) )
             {
                 $pres = Prestation::where("id","=",$nombre)->first();
                 if($pres != NULL){
                     $tab_result[] = $nombre;
                     $nb_a_tirer--;
                 }

             }
         }
         $l = array();
         foreach ($tab_result as $value){
             $l[] = Prestation::where("id","=",$value)->first();
         }

         $prestation = Notation::groupBy('idPrestation')
             ->selectRaw('avg(note) as avg, idPrestation')->orderBy("avg",'DESC')->take(4)
             ->lists('avg','idPrestation');
         $best = array();
         foreach ($prestation as $key => $value){
             $best[] = Prestation::where("id","=",$key)->first();
         }
         $table[] = $best;
         $table[] = $l;
         $vue = new VueCatalogue($table);
         if(isset($_SESSION['panier'])){
             $vue->render(5,count($_SESSION['panier']));
         }
         else{
             $vue->render(5,0);
         }
	 }

	 public function ajouterNote($idPres,$note){
		$idPres = filter_var($idPres, FILTER_SANITIZE_NUMBER_INT);
		$note = filter_var($note, FILTER_SANITIZE_NUMBER_INT);
         $app=\Slim\Slim::getInstance();
	     $all = Notation::where('ip','=',$this->get_ip())->where("idPrestation",'=',$idPres)->first();
	     if($all == NULL){
             $notation = new Notation();
             $notation->idPrestation = $idPres;
             $notation->note = $note;
             $notation->ip = $this->get_ip();
             $notation->save();
         }
         $app->redirect($_SERVER["HTTP_REFERER"]);
     }

     private function get_ip() {
         // IP si internet partagé
         if (isset($_SERVER['HTTP_CLIENT_IP'])) {
             return $_SERVER['HTTP_CLIENT_IP'];
         }
         // IP derrière un proxy
         elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
             return $_SERVER['HTTP_X_FORWARDED_FOR'];
         }
         // Sinon : IP normale
         else {
             return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
         }
     }
 }
 
