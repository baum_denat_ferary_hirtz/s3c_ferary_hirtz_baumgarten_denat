<?php

namespace giftbox\controler;
use giftbox\model\Admin;
use \giftbox\model\Categorie;
use giftbox\model\Notation;
use \giftbox\model\Prestation;
use giftbox\model\PrestationDesactive;
use \giftbox\vue\VueAdmin;

class ControlerAdmin
{
    public function connexionAdmin(){
        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('adminMenu');
        $vue = new VueAdmin();
        if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
            $mdp = filter_var($app->request->post('mdp'), FILTER_SANITIZE_STRING);
            $mdpAdmin = Admin::first();
            $adminCheck = (password_verify($mdp, $mdpAdmin['mdp']));
                if($adminCheck == true){
                    $_SESSION['admin'] = true;
                    $app->redirect($route);
                }
                else{
                    $vue->render(1);
                }
        }
        else{
            $vue->render(1);
        }
    }

    public function deconnexion(){
        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor('accueil');
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            unset($_SESSION['admin']);
            $app->redirect($route);
        }
    }

    public function menu(){
        $app=\Slim\Slim::getInstance();
        $vue = new VueAdmin();
        $vue->render(3);
    }
    public function supprimer($id){
        $app=\Slim\Slim::getInstance();
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            Prestation::where("id","=",$id)->delete();
            Notation::where("idPrestation","=",$id)->delete();
            $app->redirect($_SERVER["HTTP_REFERER"]);
        }
        //Page d'erreur
    }

    public function desactiver($id){
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $app=\Slim\Slim::getInstance();
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $pres = Prestation::where("id","=",$id)->first();
            if($pres != NULL){
            $PresDesactive = new PrestationDesactive();
            $PresDesactive->id = $pres->id;
            $PresDesactive->desc = $pres->descr;
            $PresDesactive->img = $pres->img;
            $PresDesactive->nom = $pres->nom;
            $PresDesactive->cat_id = $pres->cat_id;
            $PresDesactive->prix = $pres->prix;
            $PresDesactive->save();
            Prestation::where("id","=",$id)->delete();
            Notation::where("idPrestation","=",$id)->delete();
            $app->redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        //Page d'erreur
    }

    public function reactiver($id){
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $app=\Slim\Slim::getInstance();
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)){
            $pres = PrestationDesactive::where("id","=",$id)->first();
            if($pres != NULL){
                $PresDesactive = new Prestation();
                $PresDesactive->id = $pres->id;
                $PresDesactive->descr = $pres->desc;
                $PresDesactive->img = $pres->img;
                $PresDesactive->nom = $pres->nom;
                $PresDesactive->cat_id = $pres->cat_id;
                $PresDesactive->prix = $pres->prix;
                $PresDesactive->save();
                PrestationDesactive::where("id","=",$id)->delete();
                $app->redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        //Page d'erreur
    }

    public function gestionAdmin(){
        $l = Prestation::all();
        $vue = new VueAdmin($l);
        $vue->render(2);
    }

    public function presDesactive(){
        $l = PrestationDesactive::all();
        $vue = new VueAdmin($l);
        $vue->render(4);
    }

    public function ajoutPrestation(){
        $app=\Slim\Slim::getInstance();
        $route = $app->urlFor("adminMenu");
        if((isset($_SESSION['admin'])) && ($_SESSION['admin'] == true)) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $cat = array('Attention', 'Activité', 'Restauration', 'Hébergement');
                $nom = filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
                $descr = filter_var($app->request->post('descr'), FILTER_SANITIZE_STRING);
                $prix = filter_var($app->request->post('prix'), FILTER_SANITIZE_NUMBER_FLOAT);
                $categorie = array_search(filter_var($app->request->post('descr'), FILTER_SANITIZE_STRING), $cat) + 1;
                $image = $_FILES['image']['name'];
                $dossier = "../img/".basename($_FILES['image']['name']);
                $this->upload();
                $prestation = new Prestation();
                $prestation->nom = $nom;
                $prestation->descr = $descr;
                $prestation->img = $image;
                $prestation->cat_id = $categorie;
                $prestation->prix = $prix;
                $prestation->save();
                $app->redirect($route);
            }
        }
        $vue = new VueAdmin();
        $vue->render(5);
    }

    private function uploadBis($index,$destination,$maxsize=FALSE,$extensions=FALSE){
        //Test1: fichier correctement uploadé
        if (!isset($_FILES[$index]) OR $_FILES[$index]['error'] > 0) return FALSE;
        //Test2: taille limite
        if ($maxsize !== FALSE AND $_FILES[$index]['size'] > $maxsize) return FALSE;
        //Test3: extension
        $ext = substr(strrchr($_FILES[$index]['name'],'.'),1);
        if ($extensions !== FALSE AND !in_array($ext,$extensions)) return FALSE;
        //Déplacement
        return move_uploaded_file($_FILES[$index]['tmp_name'],$destination);
    }

    private function upload(){
        $target_dir = "img/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["image"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}