<?php

namespace giftbox\controler;
use \giftbox\model\Categorie;
use \giftbox\model\Prestation;
use \giftbox\model\Coffret;
use \giftbox\model\Contient;
use \giftbox\model\Cagnotte;
use \giftbox\vue\VuePanier;


class ControlerPanier
{
    function ajoutPanier($id){
        $app=\Slim\Slim::getInstance();
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $recherche = false;
        if(!isset($_SESSION['panier'])){
            $_SESSION['panier'] = array("$id" => 1);
        }
        else{
            foreach ($_SESSION['panier'] as $key => $value){
                if($key == $id){
                    $recherche = true;
                }
            }
            if($recherche){
                $_SESSION['panier'][$id] = $_SESSION['panier'][$id] + 1;
            }
            else{
                $_SESSION['panier'][$id] = 1;
            }
        }

        $url = $_SERVER['HTTP_REFERER'];
        $route = $app->urlFor('prestations');
        $app->redirect($url);
    }

    function panier(){
        $app=\Slim\Slim::getInstance();
        $table = array();
        if(isset($_SESSION['panier'])){
            $quantite = $_SESSION['panier'];
            foreach ($_SESSION['panier'] as $key => $value) {
                $table[] = Prestation::where("id","=",$key)->first();
            }
            $vue = new VuePanier($table,$quantite);
            $vue->render(1, count($_SESSION['panier']));
        }else{
            $vue = new VuePanier(array());
            $vue->render(1);
        }
    }

    function changerQuantite($id,$operation){
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);		
		$operation = filter_var($operation, FILTER_SANITIZE_STRING);
        $app=\Slim\Slim::getInstance();
        if($operation == "plus"){
            $_SESSION['panier'][$id] = $_SESSION['panier'][$id] + 1;
        }
        else if($operation == "moins"){
            if ($_SESSION['panier'][$id] > 0){
                $_SESSION['panier'][$id] = $_SESSION['panier'][$id] - 1;
                if($_SESSION['panier'][$id] == 0){
                    unset($_SESSION['panier'][$id]);
                    if(count($_SESSION['panier']) == 0){
                        unset($_SESSION['panier']);
                    }
                }
            }
        }
        if($operation == "supp"){
            unset($_SESSION['panier'][$id]);
            if(count($_SESSION['panier']) == 0){
                unset($_SESSION['panier']);
            }
        }
        $route = $app->urlFor('panier');
        $app->redirect($route);
    }
	
	function generer_url($nbr){
    $res = "";
    $chaine = "abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSUTVWXYZ0123456789";
    $nb_chars = strlen($chaine);
    for($i=0; $i<$nbr; $i++)
    {
        $res .= $chaine[ rand(0, ($nb_chars-1)) ];
    }
    return $res;
	}
	
	function validerPanier(){
		$app=\Slim\Slim::getInstance();
		if(isset($_SESSION['panier'])){
            $quantite = $_SESSION['panier'];
            foreach ($_SESSION['panier'] as $key => $value) {
                $table[] = Prestation::where("id","=",$key)->first();
            }
            $vue = new VuePanier($table,$quantite);
            $vue->render(2, count($_SESSION['panier']));
        }else{
            $vue = new VuePanier(array());
            $vue->render(2);
        }
		
		if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
			$nom = filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
			$prenom = filter_var($app->request->post('prenom'), FILTER_SANITIZE_STRING);
			$email = filter_var($app->request->post('email'), FILTER_SANITIZE_EMAIL);
			$msg = filter_var($app->request->post('message'), FILTER_SANITIZE_STRING);
			$mdp = filter_var($app->request->post('mdp'), FILTER_SANITIZE_STRING);
			$prix = $app->request->post('total');
			$paiement = $app->request->post('paiement');
			$hash = password_hash($mdp, PASSWORD_DEFAULT);
			
			$gestion = $this->generer_url(15);
			$urlgestion = "http://".$_SERVER['SERVER_NAME']."/connexion/".$gestion;
						
			$coffret = new Coffret();
			$coffret->prixtotal = $prix;
			$coffret->modepaiement = $paiement;
			$coffret->nom = $nom;
			$coffret->prenom = $prenom;
			$coffret->mail = $email;	
			$coffret->urlgestion = $urlgestion;
			$coffret->mdp = $hash;
						
			if($paiement == 2){
				$cadeau = 0;
				$cagnotte = $this->generer_url(15);
				$urlcagnotte = "http://".$_SERVER['SERVER_NAME']."/cagnotte/".$cagnotte;
				$coffret->urlcagnotte = $urlcagnotte;
				$c = new Cagnotte();
				$c->urlcagnotte = $urlcagnotte;
				$c->save();
			}
			else{
				$cagnotte = 0;				
				$coffret->etatpaiement = 1;
			}		
							
			if($msg != NULL){		
				$coffret->message = $msg;
			}
			
			$cadeau = $this->generer_url(15);
			$urlcadeau = "http://".$_SERVER['SERVER_NAME']."/cadeau/".$cadeau;
			$coffret->urlcadeau = $urlcadeau;			
			$coffret->save();
			
			$idc = $coffret->idcoffret;
			
			foreach($_SESSION['panier'] as $key => $value){
				$contient = new Contient();
				$contient->idcoffret = $idc;
				$contient->id = $key;
				$contient->save();
			}
			
			$_SESSION['idcoffret'] = $coffret->idcoffret;
			$confirmation = $app->urlFor('confirmation');
			$saisirpaiement = $app->urlFor('paiement');
			if($paiement == 1){
				$app->redirect($saisirpaiement);
				if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
					$num = filter_var($app->request->post('num'), FILTER_SANITIZE_NUMBER_INT);					
					$crypt = filter_var($app->request->post('crypt'), FILTER_SANITIZE_NUMBER_INT);
					$d = filter_var($app->request->post('date'), FILTER_SANITIZE_STRING);
					
					$coffret->num = $num;
					$coffret->crypt = $crypt;
					$coffret->datevalidite = $d;
					$coffret->save();
					
					if($_POST['validerpaiement'] == 'Valider paiement'){
						$app->redirect($confirmation);						
					}
					
				}				
			}
			else{
				$app->redirect($confirmation);
			}
		}	
	}
	
	function confirmerCommande(){
		$l = Coffret::select('urlcadeau', 'urlgestion', 'urlcagnotte')->where('idcoffret','=', $_SESSION['idcoffret'])->first();
		$vue = new VuePanier($l);
		unset($_SESSION['panier']);
        $vue->render(3);
	}
	
	function saisirPaiement(){
		$app=\Slim\Slim::getInstance();
		$l = Coffret::where('idcoffret','=', $_SESSION['idcoffret'])->first();
		$vue = new VuePanier($l);
        $vue->render(4);
			if($_SERVER[ 'REQUEST_METHOD' ] === 'POST'){
				$confirmation = $app->urlFor('confirmation');
				$app->redirect($confirmation);
			}
	}
}