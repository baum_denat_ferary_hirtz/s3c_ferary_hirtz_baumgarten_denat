<?php
namespace giftbox\model;

class Admin extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'admin';
    protected $primaryKey = 'pseudo';
    public $timestamps = false;
}