<?php
namespace giftbox\model;

class Notation extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'notation';
    protected $primaryKey = 'idPrestation';
    public $timestamps = false;

}
