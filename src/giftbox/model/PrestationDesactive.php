<?php
namespace giftbox\model;

class PrestationDesactive extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'prestationDesactive';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function categorie(){
        return $this->belongsTo('\giftbox\model\Categorie', 'cat_id');
    }

    public function note(){
        return $this->hasMany('\giftbox\model\Notation', 'idPrestation');
    }
}
