<?php
namespace giftbox\model;

class Cagnotte extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'cagnotte';
	protected $primaryKey = 'urlcagnotte';
	public $timestamps = false;
	
	public function coffret(){
		return $this->belongsTo('\giftbox\model\Coffret', 'urlcagnotte');
	}
}