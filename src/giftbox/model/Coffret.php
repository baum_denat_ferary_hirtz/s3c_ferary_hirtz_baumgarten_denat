<?php
namespace giftbox\model;

class Coffret extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'coffret';
	protected $primaryKey = 'idcoffret';
	public $timestamps = false;
	
}