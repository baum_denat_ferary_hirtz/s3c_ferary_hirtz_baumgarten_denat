-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 18 Janvier 2017 à 19:16
-- Version du serveur :  5.6.33
-- Version de PHP :  7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `giftbox`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `mdp`) VALUES
(1, '$2y$12$o5D6re.Xw7TTLGOptlxhIeop6Ei.KHIm9l30qEXcWlg/0gWaUlYz2');

-- --------------------------------------------------------

--
-- Structure de la table `cagnotte`
--

CREATE TABLE `cagnotte` (
  `urlcagnotte` varchar(255) NOT NULL,
  `montant` int(11) NOT NULL DEFAULT '0',
  `cloture` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cagnotte`
--

INSERT INTO `cagnotte` (`urlcagnotte`, `montant`, `cloture`) VALUES
('http://localhost/Giftbox/cagnotte/aQO9QP2F9jL23yu', 0, 0),
('http://localhost/Giftbox/cagnotte/r0bd8erXde0VUr6', 12, 0);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Attention'),
(2, 'Activité'),
(3, 'Restauration'),
(4, 'Hébergement');

-- --------------------------------------------------------

--
-- Structure de la table `coffret`
--

CREATE TABLE `coffret` (
  `idcoffret` int(11) NOT NULL,
  `prixtotal` float NOT NULL,
  `urlgestion` varchar(255) DEFAULT NULL,
  `urlcadeau` varchar(255) DEFAULT NULL,
  `urlcagnotte` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `modepaiement` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `crypt` int(11) DEFAULT NULL,
  `datevalidite` date DEFAULT NULL,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `mail` varchar(60) NOT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `etatpaiement` int(11) NOT NULL DEFAULT '0',
  `etatcadeau` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `coffret`
--

INSERT INTO `coffret` (`idcoffret`, `prixtotal`, `urlgestion`, `urlcadeau`, `urlcagnotte`, `message`, `modepaiement`, `num`, `crypt`, `datevalidite`, `nom`, `prenom`, `mail`, `mdp`, `etatpaiement`, `etatcadeau`) VALUES
(74, 39, 'http://localhost/Giftbox/connexion/fe32Vv0EOMyre9n', 'http://localhost/Giftbox/cadeau/XH3BDR6KnS1xEvI', NULL, 'Je suis beau', 1, NULL, NULL, NULL, 'Thomas', 'Ferary', 'thomas.ferary@yahoo.com', '$2y$10$YDNYgqyo0vNAUKHfrbmofOCbaSmZgdLgZ.kFmoDpS/rZ6KIx52BMm', 1, 1),
(75, 34, 'http://localhost/Giftbox/connexion/1BMTsxfh1TTpaYp', 'http://localhost/Giftbox/cadeau/pMWhcnZ9XcSPBk9', NULL, 'zazad', 1, NULL, NULL, NULL, 'Thomas', 'Ferary', 'azaze@yahiii', '$2y$10$ANEuziV50E2Zj7I4BUcpLOb8XyEwuEHzM94/Lj3q9aJ1SNf0Iufd2', 1, 1),
(76, 0, 'http://localhost/Giftbox/connexion/tWncnYZmSg3GZbV', 'http://localhost/Giftbox/cadeau/gSfvTtiJH1QAxFB', 'http://localhost/Giftbox/cagnotte/aQO9QP2F9jL23yu', 'azeazeaze', 2, NULL, NULL, NULL, 'azaze', 'azeaze', 'azeaze@azaze', '$2y$10$.Qf1X7EyOReAsEyhr9HrTeMQgL.AfXyk.R68wDtn7RfI8.BSinZi6', 0, 0),
(77, 39, 'http://localhost/Giftbox/connexion/7bBKWU621XMLcMA', 'http://localhost/Giftbox/cadeau/Up7iYT1VMSJmtLY', 'http://localhost/Giftbox/cagnotte/r0bd8erXde0VUr6', 'ezfzef', 2, NULL, NULL, NULL, 'zearz', 'raer', 'UnityRazeraFS@fezf', '$2y$10$btQBLYjAPEg7Zy73iaouhuyfE6xZ4oDjdBWLnWnAOYI1MMLBpPQ32', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `contient`
--

CREATE TABLE `contient` (
  `id` int(11) NOT NULL,
  `idcoffret` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contient`
--

INSERT INTO `contient` (`id`, `idcoffret`) VALUES
(2, 74),
(3, 74),
(3, 75),
(4, 75),
(2, 77),
(3, 77);

-- --------------------------------------------------------

--
-- Structure de la table `notation`
--

CREATE TABLE `notation` (
  `idPrestation` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `notation`
--

INSERT INTO `notation` (`idPrestation`, `note`, `ip`) VALUES
(3, 3, '::1'),
(1, 4, '::1'),
(8, 3, '::1'),
(22, 5, '::1'),
(11, 4, '::1'),
(2, 4, '::1'),
(4, 5, '::1'),
(1, 2, '::2');

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE `prestation` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `prestation`
--

INSERT INTO `prestation` (`id`, `nom`, `descr`, `cat_id`, `img`, `prix`) VALUES
(1, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 1, 'champagne.jpg', '20.00'),
(2, 'Musique', 'Partitions de piano à 4 mains', 1, 'musique.jpg', '25.00'),
(3, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 2, 'poirelregarder.jpg', '14.00'),
(4, 'Goûter', 'Goûter au FIFNL', 3, 'gouter.jpg', '20.00'),
(5, 'Projection', 'Projection courts-métrages au FIFNL', 2, 'film.jpg', '10.00'),
(6, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 1, 'rose.jpg', '16.00'),
(7, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 3, 'bonroi.jpg', '60.00'),
(8, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 3, 'origami.jpg', '12.00'),
(9, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 1, 'bricolage.jpg', '24.00'),
(10, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 3, 'grandrue.jpg', '59.00'),
(11, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 2, 'place.jpg', '11.00'),
(12, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 1, 'bijoux.jpg', '29.00'),
(13, 'Opéra', 'Concert commenté à l’Opéra', 2, 'opera.jpg', '15.00'),
(14, 'Thé Hotel de la reine', 'Thé de debriefing au bar de l’Hotel de la reine', 3, 'hotelreine.gif', '5.00'),
(15, 'Jeu connaissance', 'Jeu pour faire connaissance', 2, 'connaissance.jpg', '6.00'),
(16, 'Diner', 'Diner (Apéritif / Plat / Vin / Dessert / Café)', 3, 'diner.jpg', '40.00'),
(17, 'Cadeaux individuels', 'Cadeaux individuels sur le thème de la soirée', 1, 'cadeaux.jpg', '13.00'),
(18, 'Animation', 'Activité animée par un intervenant extérieur', 2, 'animateur.jpg', '9.00'),
(19, 'Jeu contacts', 'Jeu pour échange de contacts', 2, 'contact.png', '5.00'),
(20, 'Cocktail', 'Cocktail de fin de soirée', 3, 'cocktail.jpg', '12.00'),
(21, 'Star Wars', 'Star Wars - Le Réveil de la Force. Séance cinéma 3D', 2, 'starwars.jpg', '12.00'),
(22, 'Concert', 'Un concert à Nancy', 2, 'concert.jpg', '17.00'),
(23, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 4, 'apparthotel.jpg', '56.00'),
(24, 'Hôtel d\'Haussonville', 'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 4, 'hotel_haussonville_logo.jpg', '169.00'),
(25, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 2, 'boitedenuit.jpg', '32.00'),
(26, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 2, 'laser.jpg', '15.00'),
(27, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.', 2, 'fort.jpg', '25.00'),
(28, 'Uno', 'Jeu de carte', 1, 'uno.jpg', '30.00');

-- --------------------------------------------------------

--
-- Structure de la table `prestationDesactive`
--

CREATE TABLE `prestationDesactive` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `desc` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cagnotte`
--
ALTER TABLE `cagnotte`
  ADD PRIMARY KEY (`urlcagnotte`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `coffret`
--
ALTER TABLE `coffret`
  ADD PRIMARY KEY (`idcoffret`);

--
-- Index pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prestationDesactive`
--
ALTER TABLE `prestationDesactive`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `coffret`
--
ALTER TABLE `coffret`
  MODIFY `idcoffret` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT pour la table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;