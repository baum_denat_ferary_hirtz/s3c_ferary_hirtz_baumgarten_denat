INSTALLATION DU PROJET SUR UNE MACHINE :

- cloner le dépôt

- créer la base de données : exécuter la requête "giftbox.sql" présente dans les sources (sur phpMyAdmin par exemple)

- ajouter un fichier "conf.ini" dans Giftbox/src/conf. Ce fichier doit contenir les informations suivantes : driver =mysql, host = , database=giftbox, username = , password = , charset =utf8, collation =utf8_unicode_ci.

- pour accéder à l'espace administrateur vous devez saisir le mot de passe "admin" (celui-ci est bien entendu très simple et peu sûr, mais il a été créé spécialement pour vous permettre d'accéder aux fonctionnalités du mode administrateur de façon facilitée)

CONTRIBUTEURS

Thomas Ferary, Mylène Hirtz, Thomas Baumgarten, Martin Denat Groupe : S3C