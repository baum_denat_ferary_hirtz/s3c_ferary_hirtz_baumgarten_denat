function responsive() {
    var x = document.getElementById("mobile");
    var y = document.getElementById("mobileLogo");
    if (x.className === "navigation") {
        x.className += " responsive";
        y.style.transform = 'rotate(180deg)';
    }
    else {
        x.className = "navigation";
        y.style.transform = 'rotate(-0deg)';
    }
}